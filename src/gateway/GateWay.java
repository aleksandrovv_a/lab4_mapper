package gateway;

public class GateWay {
    private Double amount;
    private String category;
    private String description;


    public GateWay(Double amount, String category, String description) {
        this.amount = amount;
        this.category = category;
        this.description = description;

    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
