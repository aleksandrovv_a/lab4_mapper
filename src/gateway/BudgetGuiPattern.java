package gateway;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.ui.RectangleEdge;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

public class BudgetGuiPattern {
    private JFrame frame;
    private JPanel graphicPanel;
    private JComboBox<String> categories;
    private JTextField amountField;
    private  JTextField descriptionField;
    private  JButton addButton;
    private JButton showStatisticsButton;
    private JButton showButton;
    private JButton showButtonCategory;
    private  JTable table;

    public BudgetGuiPattern(){
        initialize();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new BudgetGuiPattern();
            }

        });
    }


    private void initialize(){
        frame = new JFrame();
        frame.setTitle("Budget Tracker");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(1000, 800);
        frame.setLayout(new BorderLayout());

        JPanel inputPanel = new JPanel(new FlowLayout());
        amountField = new JTextField(10);
        descriptionField = new JTextField(10);
        addButton = new JButton("Add Transaction");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addTransaction();
            }
        });

        inputPanel.add(new JLabel("Price: "));
        inputPanel.add(amountField);
        inputPanel.add(new JLabel("Description: "));
        inputPanel.add(descriptionField);
        categories = new JComboBox<>();
        categories.addItem("Food");
        categories.addItem("Beauty");
        categories.addItem("Entertainment");
        categories.addItem("Clothes");
        inputPanel.add(categories);
        inputPanel.add(addButton);
        frame.add(inputPanel, BorderLayout.NORTH);

        table = new JTable();
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(500, 600));
        frame.add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        showButton = new JButton("Show Transactions");
        showButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showTransactions();
            }
        });

        JButton deleteAllButton = new JButton("Delete All Transactions");
        deleteAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteAllTransactions();
            }
        });

        showStatisticsButton = new JButton("Show Statistics"); // Создаем кнопку
        showStatisticsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showStatistics();
                String selectedCategory = (String) categories.getSelectedItem();
                showStatisticsCategory(selectedCategory);
            }
        });

        showButtonCategory = new JButton("Show Transactions by Category");
        showButtonCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showTransactionsByCategory();
            }
        });

        buttonPanel.add(showButtonCategory);
        buttonPanel.add(showButton);
        buttonPanel.add(deleteAllButton);
        buttonPanel.add(showStatisticsButton);
        frame.add(buttonPanel, BorderLayout.SOUTH);

        graphicPanel = new JPanel(new BorderLayout());
        graphicPanel.setPreferredSize(new Dimension(500, 600));
        frame.add(graphicPanel, BorderLayout.EAST);
        frame.setVisible(true);

    }
    private void deleteAllTransactions() {
        try {
            GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
            mapper.deleteAllTransactions();
            mapper.close();
            JOptionPane.showMessageDialog(frame, "All transactions deleted successfully.");
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void addTransaction() {
        try {
            double amount = Double.parseDouble(amountField.getText());
            String description = descriptionField.getText();
            String category = (String) categories.getSelectedItem();
            GateWay transaction = new GateWay(amount, category, description);
            GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
            mapper.saveTransaction(transaction);
            mapper.close();
            amountField.setText("");
            descriptionField.setText("");
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showTransactions() {

        try {
            GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
            GateWay[] transactions = mapper.getAllTransactions();
            mapper.close();
            DefaultTableModel model = new DefaultTableModel();

            model.addColumn("Price");
            model.addColumn("Category");
            model.addColumn("Description");

            JTableHeader header = table.getTableHeader();
            header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
            header.setBackground(Color.CYAN);
            header.setForeground(Color.BLACK);

            model.setRowCount(0);

            for (GateWay transaction : transactions) {
                Object[] row = {transaction.getAmount(), transaction.getCategory(), transaction.getDescription()};
                model.addRow(row);
            }
            table.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    Color[] colors = {Color.CYAN, Color.pink, Color.orange, Color.green, Color.lightGray, Color.blue, Color.red, Color.magenta, Color.yellow};
    private void showStatistics() {
        try {
            GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
            GateWay[] transactions = mapper.getAllTransactions();
            mapper.close();

            Map<String, Double> categoryAmountMap = new HashMap<>();

            // Вычисляем сумму транзакций для каждой категории
            for (GateWay transaction : transactions) {
                String category = transaction.getCategory();
                double amount = transaction.getAmount();
                categoryAmountMap.put(category, categoryAmountMap.getOrDefault(category, 0.0) + amount);
            }

            DefaultPieDataset dataset = new DefaultPieDataset();
            for (Map.Entry<String, Double> entry : categoryAmountMap.entrySet()) {
                dataset.setValue(entry.getKey(), entry.getValue());
            }
            
            JFreeChart chart = ChartFactory.createPieChart(
                    "Transaction Statistics",
                    dataset,
                    true,
                    true,
                    false
            );

            PiePlot plot = (PiePlot) chart.getPlot();
            int colorIndex = 0;
            for (Map.Entry<String, Double> entry : categoryAmountMap.entrySet()) {
                if (colorIndex < colors.length) {
                    plot.setSectionPaint(entry.getKey(), colors[colorIndex]);
                }
                colorIndex++;
            }

            Font labelFont = plot.getLabelFont();
            Font newLableFont = labelFont.deriveFont(labelFont.getSize() * 1.2f);

            Font titleFont = chart.getTitle().getFont();
            Font newTitleFont = titleFont.deriveFont(titleFont.getSize() * 0.8f);
            chart.getTitle().setFont(newTitleFont);

            chart.removeLegend();
            LegendTitle legend = new LegendTitle(chart.getPlot());
            legend.setPosition(RectangleEdge.RIGHT);
            chart.addLegend(legend);

            plot.setLabelFont(newLableFont);
            plot.setBackgroundPaint(Color.WHITE);
            plot.setLabelBackgroundPaint(Color.WHITE);

            ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new Dimension(350, 300));
            graphicPanel.removeAll();
            graphicPanel.add(chartPanel, BorderLayout.NORTH);
            graphicPanel.revalidate();

        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showStatisticsCategory(String category){
        try{
            GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
            GateWay[] transactions = mapper.getTransactionsByCategory(category);
            mapper.close();

            DefaultPieDataset dataset = new DefaultPieDataset();
            for (GateWay transaction: transactions) {
                dataset.setValue(transaction.getDescription(), transaction.getAmount());
            }

            JFreeChart chart = ChartFactory.createPieChart(
                    "Transaction Statistics for Category: " + category,
                    dataset,
                    true,
                    true,
                    false
            );

            PiePlot plot = (PiePlot) chart.getPlot();
            int colorIndex = 0;
            for (GateWay transaction: transactions) {
                if (colorIndex < colors.length) {
                    plot.setSectionPaint(transaction.getDescription(), colors[colorIndex]);
                }
                colorIndex++;
            }

            Font labelFont = plot.getLabelFont();
            Font newLableFont = labelFont.deriveFont(labelFont.getSize() * 1.1f);

            Font titleFont = chart.getTitle().getFont();
            Font newTitleFont = titleFont.deriveFont(titleFont.getSize() * 0.8f);
            chart.getTitle().setFont(newTitleFont);

            chart.removeLegend();
            LegendTitle legend = new LegendTitle(chart.getPlot());
            legend.setPosition(RectangleEdge.RIGHT);
            chart.addLegend(legend);

            plot.setLabelFont(newLableFont);
            plot.setBackgroundPaint(Color.WHITE);
            plot.setLabelBackgroundPaint(Color.WHITE);


            ChartPanel chartPanel = new ChartPanel(chart);
            chartPanel.setPreferredSize(new Dimension(300, 300));
            graphicPanel.add(chartPanel, BorderLayout.SOUTH);
            graphicPanel.revalidate();

        } catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

//    private void showTransactionsByCategory() {
//        try {
//            String selectedCategory = (String) categories.getSelectedItem();
//            TransactionMapper mapper = new TransactionMapper("jdbc:sqlite:budget.db");
//            Transaction[] transactions = mapper.getTransactionsByCategory(selectedCategory);
//            mapper.close();
//
//            DefaultTableModel model = new DefaultTableModel();
//            model.addColumn("Price");
//            model.addColumn("Category");
//            model.addColumn("Description");
//
//            for (Transaction transaction : transactions) {
//                Object[] row = {transaction.getAmount(), transaction.getCategory(), transaction.getDescription()};
//                model.addRow(row);
//            }
//            table.setModel(model);
//        } catch (Exception e) {
//            e.printStackTrace();
//            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//        }
//    }


    private void showTransactionsByCategory() {
        JPanel categoryPanel = new JPanel();
        categoryPanel.setLayout(new GridLayout(0, 1));

        JComboBox<String> categoryComboBox = new JComboBox<>();
        categoryComboBox.addItem("Food");
        categoryComboBox.addItem("Beauty");
        categoryComboBox.addItem("Entertainment");
        categoryComboBox.addItem("Clothes");

        int result = JOptionPane.showConfirmDialog(frame, categoryComboBox, "Select Category", JOptionPane.OK_CANCEL_OPTION);

        if (result == JOptionPane.OK_OPTION) {
            try {
                GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
                DefaultTableModel model = new DefaultTableModel();
                model.addColumn("Price");
                model.addColumn("Category");
                model.addColumn("Description");

                String selectedCategory = (String) categoryComboBox.getSelectedItem();
                GateWay[] selectedTransactions = mapper.getTransactionsByCategory(selectedCategory);
                for (GateWay transaction : selectedTransactions) {
                        model.addRow(new Object[]{transaction.getAmount(), transaction.getCategory(), transaction.getDescription()});
                    }

                JTableHeader header = table.getTableHeader();
                header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
                header.setBackground(Color.CYAN);
                header.setForeground(Color.BLACK);

                table.setModel(model);

                mapper.close();
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }


}
