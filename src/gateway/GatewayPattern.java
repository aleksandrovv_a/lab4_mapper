package gateway;

import gateway.GateWay;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GatewayPattern {
    private Connection connection;
    private Statement statement;

    public GatewayPattern(String dbUrl) {
        try {
            connection = DriverManager.getConnection(dbUrl);
            statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS transactions " +
                    "(id INTEGER PRIMARY KEY, amount REAL, category TEXT, description TEXT)");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void saveTransaction(GateWay transaction) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "INSERT INTO transactions (amount, category, description) VALUES (?, ?, ?)");
            preparedStatement.setDouble(1, transaction.getAmount());
            preparedStatement.setString(2, transaction.getCategory());
            preparedStatement.setString(3, transaction.getDescription());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public GateWay[] getAllTransactions() {
        List<GateWay> transactions = new ArrayList<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions");

            while (resultSet.next()) {
                double amount = resultSet.getDouble("amount");
                String category = resultSet.getString("category");
                String description = resultSet.getString("description");
                transactions.add(new GateWay(amount, category, description));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return transactions.toArray(new GateWay[0]);
    }

    public GateWay[] getTransactionsByCategory(String category) {
        try {
            // Сначала подсчитываем количество транзакций
            PreparedStatement countStatement = connection.prepareStatement(
                    "SELECT COUNT(*) FROM transactions WHERE category=?");
            countStatement.setString(1, category);
            ResultSet countResult = countStatement.executeQuery();
            countResult.next();
            int rowCount = countResult.getInt(1);

            // Затем получаем сами транзакции
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT * FROM transactions WHERE category=?");
            preparedStatement.setString(1, category);
            ResultSet resultSet = preparedStatement.executeQuery();

            GateWay[] transactions = new GateWay[rowCount];
            int index = 0;
            while (resultSet.next()) {
                double amount = resultSet.getDouble("amount");
                String description = resultSet.getString("description");
                transactions[index++] = new GateWay(amount, category, description);
            }
            return transactions;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void deleteAllTransactions() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM transactions");
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void close() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}


