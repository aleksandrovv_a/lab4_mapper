package gateway;

import java.sql.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BudgetGuiWithoutPattern {
    private JFrame frame;
    private JPanel graphicPanel;
    private JComboBox<String> categories;
    private JTextField amountField;
    private JTextField descriptionField;
    private JButton addButton;
    private JButton showStatisticsButton;
    private JButton showButton;
    private JButton showButtonCategory;
    private JTable table;

    private Connection connection;

    public BudgetGuiWithoutPattern() {
        initialize();
        connectToDatabase("jdbc:sqlite:budget.db");
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new BudgetGuiWithoutPattern();
            }
        });
    }

    private void initialize() {
        frame = new JFrame();
        frame.setTitle("Budget Tracker");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(800, 600);
        frame.setLayout(new BorderLayout());

        JPanel inputPanel = new JPanel(new FlowLayout());
        amountField = new JTextField(10);
        descriptionField = new JTextField(10);
        addButton = new JButton("Add Transaction");
        addButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                addTransaction();
            }
        });
        inputPanel.add(new JLabel("Price: "));
        inputPanel.add(amountField);
        inputPanel.add(new JLabel("Description: "));
        inputPanel.add(descriptionField);
        categories = new JComboBox<>();
        categories.addItem("Food");
        categories.addItem("Beauty");
        categories.addItem("Entertainment");
        categories.addItem("Clothes");
        inputPanel.add(categories);
        inputPanel.add(addButton);
        frame.add(inputPanel, BorderLayout.NORTH);

        table = new JTable();
        JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setPreferredSize(new Dimension(400, 400));
        frame.add(scrollPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new FlowLayout());
        showButton = new JButton("Show Transactions");
        showButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showTransactions();
            }
        });

        JButton deleteAllButton = new JButton("Delete All Transactions");
        deleteAllButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteAllTransactions();
            }
        });

        showStatisticsButton = new JButton("Show Statistics");
        showStatisticsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showStatistics();
                String selectedCategory = (String) categories.getSelectedItem();
                showStatisticsCategory(selectedCategory);
            }
        });

        showButtonCategory = new JButton("Show Transactions by Category");
        showButtonCategory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showTransactionsByCategory();
            }
        });
        buttonPanel.add(showButtonCategory);
        buttonPanel.add(showButton);
        buttonPanel.add(deleteAllButton);
        buttonPanel.add(showStatisticsButton);
        frame.add(buttonPanel, BorderLayout.SOUTH);

        graphicPanel = new JPanel(new BorderLayout());
        graphicPanel.setPreferredSize(new Dimension(500, 600));
        frame.add(graphicPanel, BorderLayout.EAST);
        frame.setVisible(true);
    }

    private void connectToDatabase(String dbUrl) {
        try {
            connection = DriverManager.getConnection(dbUrl);
            Statement statement = connection.createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS transactions " +
                    "(id INTEGER PRIMARY KEY, amount REAL, category TEXT, description TEXT)");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void addTransaction() {
        try {
            double amount = Double.parseDouble(amountField.getText());
            String description = descriptionField.getText();
            String category = (String) categories.getSelectedItem();
            PreparedStatement insertStatement = connection.prepareStatement(
                    "INSERT INTO transactions (amount, category, description) VALUES (?, ?, ?)");
            insertStatement.setDouble(1, amount);
            insertStatement.setString(2, category);
            insertStatement.setString(3, description);
            insertStatement.executeUpdate();
            clearInputFields();
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void clearInputFields() {
        amountField.setText("");
        descriptionField.setText("");
    }

    private void showTransactions() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions");
            DefaultTableModel model = new DefaultTableModel();
            model.addColumn("Price");
            model.addColumn("Category");
            model.addColumn("Description");

            JTableHeader header = table.getTableHeader();
            header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
            header.setBackground(Color.CYAN);
            header.setForeground(Color.BLACK);

            model.setRowCount(0);

            while (resultSet.next()) {
                double amount = resultSet.getDouble("amount");
                String category = resultSet.getString("category");
                String description = resultSet.getString("description");
                model.addRow(new Object[]{amount, category, description});
            }
            table.setModel(model);
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showStatistics() {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT category, SUM(amount) AS total FROM transactions GROUP BY category");
            DefaultPieDataset dataset = new DefaultPieDataset();

            while (resultSet.next()) {
                String category = resultSet.getString("category");
                double totalAmount = resultSet.getDouble("total");
                dataset.setValue(category, totalAmount);
            }

            JFreeChart chart = ChartFactory.createPieChart(
                    "Transaction Statistics",
                    dataset,
                    true,
                    true,
                    false
            );


            ChartPanel chartPanel = new ChartPanel(chart);
            graphicPanel.removeAll();
            graphicPanel.add(chartPanel, BorderLayout.CENTER);
            graphicPanel.revalidate();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void showStatisticsCategory(String category) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT description, amount FROM transactions WHERE category=?");
            preparedStatement.setString(1, category);
            ResultSet resultSet = preparedStatement.executeQuery();
            DefaultPieDataset dataset = new DefaultPieDataset();

            while (resultSet.next()) {
                String description = resultSet.getString("description");
                double amount = resultSet.getDouble("amount");
                dataset.setValue(description, amount);
            }

            JFreeChart chart = ChartFactory.createPieChart(
                    "Transaction Statistics for Category: " + category,   // title
                    dataset,                                              // dataset
                    true,                                                 // legend
                    true,                                                 // tooltips
                    false                                                 // urls
            );

            ChartPanel chartPanel = new ChartPanel(chart);
            graphicPanel.removeAll();
            graphicPanel.add(chartPanel, BorderLayout.CENTER);
            graphicPanel.revalidate();

        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

//    private void showTransactionsByCategory() {
//        String category = (String) JOptionPane.showInputDialog(frame, "Select Category:",
//                "Transactions by Category", JOptionPane.PLAIN_MESSAGE, null,
//                new String[]{"Food", "Beauty", "Entertainment", "Clothes"}, "Food");
//        if (category != null) {
//            try {
//                PreparedStatement preparedStatement = connection.prepareStatement(
//                        "SELECT * FROM transactions WHERE category=?");
//                preparedStatement.setString(1, category);
//                ResultSet resultSet = preparedStatement.executeQuery();
//                DefaultTableModel model = new DefaultTableModel();
//                model.addColumn("Price");
//                model.addColumn("Description");
//
//                JTableHeader header = table.getTableHeader();
//                header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
//                header.setBackground(Color.CYAN);
//                header.setForeground(Color.BLACK);
//
//                model.setRowCount(0);
//
//                while (resultSet.next()) {
//                    double amount = resultSet.getDouble("amount");
//                    String description = resultSet.getString("description");
//                    model.addRow(new Object[]{amount, description});
//                }
//                table.setModel(model);
//            } catch (SQLException e) {
//                e.printStackTrace();
//                JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
//            }
//        }
//    }

    private void showTransactionsByCategory() {
        String[] categories = {"Food", "Beauty", "Entertainment", "Clothes"};
        boolean[] selectedCategories = {true, false, false, false}; // По умолчанию выбрана категория "Food"

        JPanel panel = new JPanel();
        JCheckBox[] checkBoxes = new JCheckBox[categories.length];
        for (int i = 0; i < categories.length; i++) {
            checkBoxes[i] = new JCheckBox(categories[i]);
            panel.add(checkBoxes[i]);
        }

        int result = JOptionPane.showConfirmDialog(frame, panel, "Select Category", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            try {
                DefaultTableModel model = new DefaultTableModel();
                model.addColumn("Price");
                model.addColumn("Description");

                for (int i = 0; i < categories.length; i++) {
                    if (checkBoxes[i].isSelected()) {
                        GatewayPattern mapper = new GatewayPattern("jdbc:sqlite:budget.db");
                        GateWay[] transactions = mapper.getTransactionsByCategory(categories[i]);
                        for (GateWay transaction : transactions) {
                            model.addRow(new Object[]{transaction.getAmount(), transaction.getDescription()});
                        }
                        mapper.close();
                    }
                }

                JTableHeader header = table.getTableHeader();
                header.setFont(header.getFont().deriveFont(Font.BOLD, 14f));
                header.setBackground(Color.CYAN);
                header.setForeground(Color.BLACK);

                table.setModel(model);
            } catch (Exception e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }


    private void deleteAllTransactions() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "DELETE FROM transactions");
            preparedStatement.executeUpdate();
            JOptionPane.showMessageDialog(frame, "All transactions deleted successfully.");
            showTransactions();
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(frame, "Error occurred: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}
