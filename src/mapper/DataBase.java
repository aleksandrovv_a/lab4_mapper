package mapper;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DataBase implements TransactionMapper {
    private Connection connection;

    public DataBase(String dbUrl) throws SQLException {
        connection = DriverManager.getConnection(dbUrl);
        // Создаем таблицу, если она еще не существует
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS transactions " +
                    "(id INTEGER PRIMARY KEY, amount REAL, category TEXT, description TEXT)");
        }
    }

    @Override
    public void saveTransaction(Transaction transaction) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "INSERT INTO transactions (amount, category, description) VALUES (?, ?, ?)")) {
            preparedStatement.setDouble(1, transaction.getAmount());
            preparedStatement.setString(2, transaction.getCategory());
            preparedStatement.setString(3, transaction.getDescription());
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public Transaction[] getAllTransactions() throws SQLException {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM transactions")) {
            List<Transaction> transactions = new ArrayList<>();
            while (resultSet.next()) {
                transactions.add(new Transaction(resultSet.getDouble("amount"),
                        resultSet.getString("category"),
                        resultSet.getString("description")));
            }
            return transactions.toArray(new Transaction[0]);
        }
    }

    @Override
    public Transaction[] getTransactionsByCategory(String category) throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "SELECT * FROM transactions WHERE category=?")) {
            preparedStatement.setString(1, category);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                List<Transaction> transactions = new ArrayList<>();
                while (resultSet.next()) {
                    transactions.add(new Transaction(resultSet.getDouble("amount"),
                            resultSet.getString("category"),
                            resultSet.getString("description")));
                }
                return transactions.toArray(new Transaction[0]);
            }
        }
    }

    @Override
    public void deleteAllTransactions() throws SQLException {
        try (PreparedStatement preparedStatement = connection.prepareStatement(
                "DELETE FROM transactions")) {
            preparedStatement.executeUpdate();
        }
    }

    @Override
    public void close() throws SQLException {
        connection.close();
    }

}
