package mapper;

import java.sql.SQLException;

public interface TransactionMapper {
    void saveTransaction(Transaction transaction) throws SQLException;
    Transaction[] getAllTransactions() throws SQLException;
    Transaction[] getTransactionsByCategory(String category) throws SQLException;
    void deleteAllTransactions() throws SQLException;
    void close() throws SQLException;
}
